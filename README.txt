CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Override Title, is to override or change the title of Node, Path/URL and Views pages, providing a fast and full access to Administrator. Set permission to access for the rest of the Roles throughout the site.
* Can set different title for the same page on the basis of the language, if enabled translation.
* Can check the last modified date and time.
* Track the user who lastly changed/override title of the page.
* Filter and find the content easily.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ot


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Ravi Kumar Singh (rksyravi) - https://www.drupal.org/u/rksyravi
